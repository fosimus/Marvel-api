import {
  START_LOADING,
  STOP_LOADING,
  ADD_CHARACTERS,
  SET_PAGE,
  SET_TOTAL_ITEMS,
  SET_SEARCH_VALUE
} from '~/constants/characters';

const initState = {
  isLoading: false,
  searchValue: '',
  items: [],
  pagination: {
    page: 1,
    itemsPerPage: 20,
    totalItems: 0
  }
};

export default function charactersReducer(state = initState, action) {
  switch (action.type) {
    case START_LOADING:
      return { ...state, isLoading: true };
    case STOP_LOADING:
      return { ...state, isLoading: false };
    case ADD_CHARACTERS:
      return { ...state, items: action.characters };
    case SET_PAGE:
      return {
        ...state,
        pagination: {
          ...state.pagination,
          page: action.page
        }
      };
    case SET_TOTAL_ITEMS:
      return {
        ...state,
        pagination: {
          ...state.pagination,
          totalItems: action.totalItems
        }
      };
    case SET_SEARCH_VALUE:
      return {
        ...state,
        searchValue: action.value
      };
    default:
      return state;
  }
}
