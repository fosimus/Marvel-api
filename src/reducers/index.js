import auth from './auth';
import characters from './characters';

export default {
  auth,
  characters
};
