import {
  AUTH_USER,
  UNAUTH_USER,
  AUTH_ERROR,
  START_LOADING,
  STOP_LOADING
} from '~/constants/auth';

const initState = {
  isLoading: false,
  authenticated: false,
  error: '',
  message: '',
  email: ''
};

export default function authReducer(state = initState, action) {
  switch (action.type) {
    case AUTH_USER:
      return {
        ...state,
        error: '',
        authenticated: true,
        email: action.email
      };
    case UNAUTH_USER:
      return { ...state, authenticated: false };
    case AUTH_ERROR:
      return { ...state, error: action.payload };
    case START_LOADING:
      return { ...state, isLoading: true };
    case STOP_LOADING:
      return { ...state, isLoading: false };
    default:
      return state;
  }
}
