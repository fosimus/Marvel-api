import axios from 'axios';
import { AUTH_URL } from '~/constants/config';
import {
  UNAUTH_USER,
  AUTH_USER,
  AUTH_ERROR,
  START_LOADING,
  STOP_LOADING
} from '~/constants/auth';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

export const authError = error => ({
  type: AUTH_ERROR,
  payload: error
});

const startLoading = () => ({
  type: START_LOADING
});

const stopLoading = () => ({
  type: STOP_LOADING
});

export const signinUser = ({ email, password }) => async dispatch => {
  dispatch(startLoading());
  try {
    await sleep(1500);
    const { data } = await axios.post(`${AUTH_URL}/login`, {
      email,
      password
    });

    localStorage.setItem('token', data.token);
    dispatch({ type: AUTH_USER, email });
  } catch (error) {
    dispatch(authError(`Signin error. ${error.message}.`));
  } finally {
    dispatch(stopLoading());
  }
};

export const signoutUser = () => {
  localStorage.removeItem('token');
  return {
    type: UNAUTH_USER
  };
};

export const signupUser = ({
  email,
  password,
  passwordConfirmation
}) => async dispatch => {
  dispatch(startLoading());
  try {
    await sleep(1500);
    const { data } = await axios.post(`${AUTH_URL}/signup`, {
      email,
      password,
      passwordConfirmation
    });
    localStorage.setItem('token', data.token);
    dispatch({ type: AUTH_USER, email });
  } catch (error) {
    dispatch(authError(`Signup error. ${error.message}`));
  } finally {
    dispatch(stopLoading());
  }
};
