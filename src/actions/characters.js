import axios from 'axios';
import md5 from 'md5';
import { toastr } from 'react-redux-toastr';
import { MARVEL_API } from '~/constants/config';
import {
  START_LOADING,
  STOP_LOADING,
  ADD_CHARACTERS,
  SET_PAGE,
  SET_TOTAL_ITEMS,
  SET_SEARCH_VALUE
} from '~/constants/characters';
import { privateKey, publicKey } from '../../config';

const getAuthParams = {
  apikey: publicKey,
  ts: Date.now(),
  hash: md5(Date.now() + privateKey + publicKey)
};

const startLoading = () => ({
  type: START_LOADING
});

const stopLoading = () => ({
  type: STOP_LOADING
});

const addCharacters = characters => ({
  type: ADD_CHARACTERS,
  characters
});

const setPage = page => ({
  type: SET_PAGE,
  page
});

const setTotalItems = totalItems => ({
  type: SET_TOTAL_ITEMS,
  totalItems
});

const setSearchValue = value => ({
  type: SET_SEARCH_VALUE,
  value
});

export const loadCharacters = () => async (dispatch, getStore) => {
  const {
    searchValue,
    pagination: { page, itemsPerPage }
  } = getStore().characters;

  dispatch(startLoading());

  try {
    // each API have to be checked (authentication)
    // usually, it should be hash/header or special API method
    // for this task I check auth just in Redux routing (weak defense)
    const { data } = await axios.get(`${MARVEL_API}/characters`, {
      params: {
        ...getAuthParams,
        ...(page > 1 && { offset: itemsPerPage * (page - 1) }),
        ...(searchValue && { nameStartsWith: searchValue }),
        ...(itemsPerPage && { limit: itemsPerPage })
      }
    });
    const { data: { results, total } } = data;

    dispatch(addCharacters(results));
    dispatch(setTotalItems(total));
  } catch (error) {
    toastr.error(error.message || 'Characters loading error');
  } finally {
    dispatch(stopLoading());
  }
};

export const loadNewCharacters = searchValue => async dispatch => {
  const value = searchValue || '';
  dispatch(setPage(1));
  dispatch(setSearchValue(value));
  dispatch(loadCharacters());
};

export const onPageChange = page => async dispatch => {
  dispatch(setPage(page));
  dispatch(loadCharacters());
};

export const onSearchChange = searchValue => async dispatch => {
  dispatch(loadNewCharacters(searchValue));
};
