import React, { Fragment } from 'react';
import { render } from 'react-dom';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import ReduxToastr, { reducer as toastrReducer } from 'react-redux-toastr';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import createHistory from 'history/createBrowserHistory';
import { reducer as form } from 'redux-form';
import { Route, Switch } from 'react-router';
import {
  ConnectedRouter,
  routerReducer,
  routerMiddleware
} from 'react-router-redux';

import Navigation from '~/containers/navigation';
import Content from '~/components/Content';
import Signin from '~/containers/signin';
import Signout from '~/components/Auth/Signout/';
import Signup from '~/containers/signup';
import Characters from '~/containers/characters';
import PrivateRoute from '~/components/Auth/PrivateRoute';
import Main from '~/components/Main';
import NoMatch from '~/components/NotMatch';

import reducers from './reducers';

import './index.less';
import './libs/toastr.less';

const history = createHistory();

const store = createStore(
  combineReducers({
    ...reducers,
    toastr: toastrReducer,
    routing: routerReducer,
    form
  }),
  composeWithDevTools(applyMiddleware(routerMiddleware(history), thunk))
);

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Fragment>
        <Navigation />
        <Content>
          <Switch>
            <Route path="/signin" component={Signin} />
            <Route path="/signout" component={Signout} />
            <Route path="/signup" component={Signup} />
            <PrivateRoute path="/characters" exact component={Characters} />
            <Route path="/" exact component={Main} />
            <Route component={NoMatch} />
          </Switch>
        </Content>
        <ReduxToastr />
      </Fragment>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('app')
);
