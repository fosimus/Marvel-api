export const AUTH_USER = 'auth/auth_user';
export const UNAUTH_USER = 'auth/unauth_user';
export const AUTH_ERROR = 'auth/auth_error';
export const START_LOADING = 'auth/start_loading';
export const STOP_LOADING = 'auth/stop_loading';
