export const START_LOADING = 'characters/start_loading';
export const STOP_LOADING = 'characters/stop_loading';
export const ADD_CHARACTERS = 'characters/add_characters';
export const SET_PAGE = 'characters/set_page';
export const SET_TOTAL_ITEMS = 'characters/set_total_items';
export const SET_SEARCH_VALUE = 'characters/set_search_value';
