import { connect } from 'react-redux';
import * as actions from '~/actions/characters';
import Characters from '~/components/Characters';

const mapStateToProps = state => ({
  isLoading: state.characters.isLoading,
  characters: state.characters.items,
  pagination: state.characters.pagination
});

export default connect(mapStateToProps, actions)(Characters);
