import { connect } from 'react-redux';
import Signup from '~/components/Auth/Signup/';
import * as actions from '~/actions/auth';

const mapStateToProps = state => ({
  authenticated: state.auth.authenticated,
  errorMessage: state.auth.error,
  isLoading: state.auth.isLoading
});

export default connect(mapStateToProps, actions)(Signup);
