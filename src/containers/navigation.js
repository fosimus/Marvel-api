import { connect } from 'react-redux';
import Navigation from '~/components/Navigation';

const mapStateToProps = state => ({
  authenticated: state.auth.authenticated,
  userEmail: state.auth.email
});

export default connect(mapStateToProps)(Navigation);
