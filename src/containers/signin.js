import { connect } from 'react-redux';
import Signin from '~/components/Auth/Signin/';
import * as actions from '~/actions/auth';

const mapStateToProps = state => ({
  authenticated: state.auth.authenticated,
  errorMessage: state.auth.error,
  isLoading: state.auth.isLoading
});

export default connect(mapStateToProps, actions)(Signin);
