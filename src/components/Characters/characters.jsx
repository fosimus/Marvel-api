import React from 'react';
import PropTypes from 'prop-types';
import Character from '~/components/Character';
import './styles.less';

const Characters = ({ characters }) => (
  <div className="characters">
    {characters.length === 0 ? (
      <div className="characters_no-items">NO CHARACTERS</div>
    ) : (
      characters.map(character => (
        <div className="characters_item" key={character.id}>
          <Character {...character} />
        </div>
      ))
    )}
  </div>
);

Characters.propTypes = {
  characters: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string
    })
  )
};

Characters.defaultProps = {
  characters: []
};

export default Characters;
