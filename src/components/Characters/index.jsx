import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Pagination from '~/components/Pagination';
import Search from '~/components/Search';
import Characters from './characters';

class CharactersPage extends Component {
  constructor(props) {
    super(props);
    this.defaultCharacters = this.getDefaultCharacters(
      props.pagination.itemsPerPage
    );
  }
  componentDidMount() {
    this.props.loadNewCharacters();
  }

  getDefaultCharacters = items =>
    [...new Array(items)].map((el, i) => ({ id: i, isFakeCharacter: true }));

  render() {
    const {
      isLoading,
      characters,
      pagination,
      onPageChange,
      onSearchChange
    } = this.props;
    return (
      <Fragment>
        <Search onSearchChange={onSearchChange} />
        <Pagination
          options={pagination}
          disabled={isLoading}
          show={characters.length > 0}
          onPageChange={onPageChange}
        />
        {isLoading ? (
          <Characters characters={this.defaultCharacters} />
        ) : (
          <Characters characters={characters} />
        )}
        <Pagination
          options={pagination}
          disabled={isLoading}
          show={characters.length > 0}
          onPageChange={onPageChange}
        />
      </Fragment>
    );
  }
}

CharactersPage.propTypes = {
  isLoading: PropTypes.bool,
  characters: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired
    })
  ),
  loadNewCharacters: PropTypes.func.isRequired,
  onPageChange: PropTypes.func.isRequired,
  onSearchChange: PropTypes.func.isRequired,
  pagination: PropTypes.shape({
    page: PropTypes.number,
    itemsPerPage: PropTypes.number,
    totalItems: PropTypes.number
  }).isRequired
};

CharactersPage.defaultProps = {
  isLoading: false,
  characters: []
};

export default CharactersPage;
