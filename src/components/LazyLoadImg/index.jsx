import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loading from '~/components/Loading';
import './styles.less';

class LazyLoadImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false
    };
  }

  componentDidMount() {
    this.mounted = true;
    const { srcLoaded } = this.props;
    const image = new window.Image();

    image.src = srcLoaded;
    image.onload = () => {
      if (this.mounted) this.setState({ loaded: true });
    };
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  render() {
    const { loaded } = this.state;
    const { srcLoaded } = this.props;
    const options = { backgroundImage: `url('${srcLoaded}'` };
    return (
      <div className="lazy-load">
        <div className="lazy-load_loader">{!loaded && <Loading isSmall />}</div>
        <div
          className={`lazy-load_image ${loaded && 'lazy-load_image-loaded'}`}
          style={loaded ? options : {}}
        />
      </div>
    );
  }
}

LazyLoadImage.propTypes = {
  srcLoaded: PropTypes.string.isRequired
};

export default LazyLoadImage;
