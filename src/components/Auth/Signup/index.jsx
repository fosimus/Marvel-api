import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import SignupForm from './Form';

class Signup extends Component {
  componentWillUnmount() {
    if (this.props.errorMessage) {
      this.props.authError(null);
    }
  }

  getRedirectPath = () => {
    const locationState = this.props.location.state;
    if (locationState && locationState.from.pathname) {
      return locationState.from.pathname;
    }
    return '/';
  };

  handleSubmit = ({ email, password, passwordConfirmation }) => {
    this.props.signupUser({ email, password, passwordConfirmation });
  };

  render() {
    const { isLoading, authenticated } = this.props;
    return authenticated ? (
      <Redirect
        to={{
          pathname: this.getRedirectPath(),
          state: {
            from: this.props.location
          }
        }}
      />
    ) : (
      <div className="flex-center">
        <SignupForm
          disabled={isLoading}
          onSubmit={this.handleSubmit}
          errorMessage={this.props.errorMessage}
        />
      </div>
    );
  }
}

Signup.propTypes = {
  authenticated: PropTypes.bool,
  authError: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  errorMessage: PropTypes.string,
  location: PropTypes.shape({
    state: PropTypes.object
  }).isRequired,
  signupUser: PropTypes.func.isRequired
};

Signup.defaultProps = {
  authenticated: false,
  isLoading: false,
  errorMessage: ''
};

export default Signup;
