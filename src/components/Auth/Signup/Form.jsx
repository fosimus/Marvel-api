import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { renderTextField } from '~/components/Auth/FormHelper/';
import { required, maxLength30, minLength3, email } from '~/helper';
import Loading from '~/components/Loading';
import './styles.less';

const SignupForm = ({ disabled, handleSubmit, errorMessage }) => (
  <form className="signup" onSubmit={handleSubmit}>
    <div className="signup_wrapper">
      <Field
        label="email"
        name="email"
        component={renderTextField}
        type="text"
        validate={[required, maxLength30, minLength3, email]}
        disabled={disabled}
      />
      <Field
        label="password"
        name="password"
        component={renderTextField}
        type="password"
        validate={[required, maxLength30, minLength3]}
        disabled={disabled}
      />
      <Field
        label="password confirmation"
        name="passwordConfirmation"
        component={renderTextField}
        type="password"
        validate={[required, maxLength30, minLength3]}
        disabled={disabled}
      />
      <div className="signup_buttons">
        {disabled ? (
          <Loading isSmall />
        ) : (
          <input
            className="signup_button input input-btn"
            type="submit"
            label="Sign Up"
          />
        )}
      </div>
    </div>
    {errorMessage && <div className="alert">{errorMessage}</div>}
  </form>
);

SignupForm.propTypes = {
  disabled: PropTypes.bool,
  errorMessage: PropTypes.string,
  handleSubmit: PropTypes.func.isRequired
};

SignupForm.defaultProps = {
  disabled: false,
  errorMessage: ''
};

const validate = values => {
  const errors = {};

  if (values.password !== values.passwordConfirmation) {
    errors.password = 'Passwords must match';
  }

  return errors;
};

export default reduxForm({
  form: 'signup',
  validate
})(SignupForm);
