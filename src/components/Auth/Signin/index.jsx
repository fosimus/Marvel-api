import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import SigninForm from './Form';

class Signin extends Component {
  componentWillUnmount() {
    if (this.props.errorMessage) {
      this.props.authError(null);
    }
  }

  getRedirectPath = () => {
    const locationState = this.props.location.state;
    if (locationState && locationState.from.pathname) {
      return locationState.from.pathname;
    }
    return '/';
  };

  handleSubmit = ({ email, password }) => {
    this.props.signinUser({ email, password });
  };

  render() {
    const { isLoading, authenticated, errorMessage, location } = this.props;
    const locationError = (location.state && location.state.message) || '';
    return authenticated ? (
      <Redirect
        to={{
          pathname: this.getRedirectPath(),
          state: {
            from: this.props.location
          }
        }}
      />
    ) : (
      <div className="flex-center">
        <SigninForm
          disabled={isLoading}
          onSubmit={this.handleSubmit}
          errorMessage={errorMessage || locationError}
        />
      </div>
    );
  }
}

Signin.propTypes = {
  authenticated: PropTypes.bool,
  authError: PropTypes.func.isRequired,
  errorMessage: PropTypes.string,
  isLoading: PropTypes.bool,
  location: PropTypes.shape({
    state: PropTypes.object
  }).isRequired,
  signinUser: PropTypes.func.isRequired
};

Signin.defaultProps = {
  isLoading: false,
  authenticated: false,
  errorMessage: ''
};

export default Signin;
