import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { required, email } from '~/helper';
import Loading from '~/components/Loading';
import { renderTextField } from '~/components/Auth/FormHelper/';
import './styles.less';

const SigninForm = ({ disabled, handleSubmit, errorMessage }) => (
  <form className="signin" onSubmit={handleSubmit}>
    <div className="signin_wrapper">
      <Field
        label="email"
        name="email"
        component={renderTextField}
        type="text"
        validate={[required, email]}
        disabled={disabled}
      />
      <Field
        label="password"
        name="password"
        component={renderTextField}
        type="password"
        validate={required}
        disabled={disabled}
      />
      <div className="signin_buttons">
        {disabled ? (
          <Loading isSmall />
        ) : (
          <input
            className="signin_button input input-btn"
            type="submit"
            label="Sign In"
          />
        )}
      </div>
    </div>
    {errorMessage && <div className="alert">{errorMessage}</div>}
  </form>
);

SigninForm.propTypes = {
  disabled: PropTypes.bool,
  errorMessage: PropTypes.string,
  handleSubmit: PropTypes.func.isRequired
};

SigninForm.defaultProps = {
  disabled: false,
  errorMessage: ''
};

export default reduxForm({
  form: 'signin'
})(SigninForm);
