import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from '~/actions/auth';
import Page from '~/components/ContentPage';

class Signout extends Component {
  componentWillMount() {
    this.props.signoutUser();
  }

  render() {
    return (
      <Page title="Bye user!">
        <div className="no-elements">Hope to see you soon</div>
      </Page>
    );
  }
}

Signout.propTypes = {
  signoutUser: PropTypes.func.isRequired
};

export default connect(null, actions)(Signout);
