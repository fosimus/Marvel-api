import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ component: ComposedComponent, ...rest }) => {
  class Authentication extends Component {
    handleRender = props => {
      if (!this.props.authenticated) {
        return (
          <Redirect
            to={{
              pathname: '/signin',
              state: {
                from: props.location,
                message: 'Please, sign in or sign up!'
              }
            }}
          />
        );
      }
      return <ComposedComponent {...props} />;
    };

    render() {
      return <Route {...rest} render={this.handleRender} />;
    }
  }

  Authentication.propTypes = {
    authenticated: PropTypes.bool
  };

  Authentication.defaultProps = {
    authenticated: false
  };

  const mapStateToProps = state => ({
    authenticated: state.auth.authenticated
  });

  const AuthenticationContainer = connect(mapStateToProps)(Authentication);
  return <AuthenticationContainer />;
};

PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired
};

export default PrivateRoute;
