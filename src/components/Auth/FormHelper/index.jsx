import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';

export const renderTextField = ({
  input,
  label,
  type,
  disabled,
  meta: { touched, error, warning }
}) => (
  <div className="field">
    <input
      className={`field_input input input-${type}`}
      {...input}
      placeholder={label}
      type={type}
      disabled={disabled}
    />
    <div className="field_notification">
      {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  </div>
);

renderTextField.propTypes = {
  input: PropTypes.shape({
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    onDragStart: PropTypes.func,
    onDrop: PropTypes.func,
    onFocus: PropTypes.func,
    value: PropTypes.string
  }).isRequired,
  disabled: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired,
  meta: PropTypes.shape({
    error: PropTypes.string,
    touched: PropTypes.bool,
    warning: PropTypes.string
  }).isRequired,
  type: PropTypes.string.isRequired
};
