import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import './styles.less';

const ModalHOC = InnerComponent =>
  class extends Component {
    static propTypes = {
      bodyProps: PropTypes.shape({}),
      trigger: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
      ]).isRequired
    };
    static defaultProps = {
      bodyProps: {}
    };
    constructor(props) {
      super(props);
      this.state = { show: false };
      this.trigger = React.cloneElement(props.trigger, {
        onClick: () => {
          // do not open modal if trigger element is prevents it
          if (!props.trigger.props.clickable) return;
          this.showModal();
        }
      });
    }

    showModal = () => {
      this.setState({ show: true });
    };

    closeModal = () => {
      this.setState({ show: false });
    };

    render() {
      const { bodyProps = {} } = this.props;
      const { show } = this.state;
      return (
        <Fragment>
          {this.trigger}
          <div className={`modal ${show ? 'modal-show' : ''}`}>
            <div className="modal_wrapper">
              <div className="modal_close">
                <input
                  className="input input-btn"
                  type="button"
                  value="x"
                  onClick={this.closeModal}
                />
              </div>
              <div className="modal_body">
                <InnerComponent
                  {...bodyProps}
                  isOpen={show}
                  closeModal={this.closeModal}
                />
              </div>
            </div>
          </div>
        </Fragment>
      );
    }
  };

export default ModalHOC;
