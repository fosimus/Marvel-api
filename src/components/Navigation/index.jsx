import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './styles.less';

const Navigation = ({ authenticated, userEmail }) => (
  <div className="nav">
    <div className="nav_wrapper">
      <Link className="nav_link nav_link-logo" to="/" />
      <Link className="nav_link" to="/characters">
        CHARACTERS<span className="nav_link_border" />
      </Link>
      <div className="nav_separator" />
      {authenticated ? (
        <Fragment>
          <Link className="nav_link" to="/signout">
            SIGN OUT
            <span className="nav_link_email">{userEmail}</span>
            <span className="nav_link_border" />
          </Link>
        </Fragment>
      ) : (
        <Fragment>
          <Link className="nav_link" to="/signin">
            SIGN IN<span className="nav_link_border" />
          </Link>
          <div className="nav_separator" />
          <Link className="nav_link" to="/signup">
            SIGN UP<span className="nav_link_border" />
          </Link>
        </Fragment>
      )}
    </div>
  </div>
);

Navigation.propTypes = {
  authenticated: PropTypes.bool,
  userEmail: PropTypes.string
};

Navigation.defaultProps = {
  authenticated: false,
  userEmail: ''
};
export default Navigation;
