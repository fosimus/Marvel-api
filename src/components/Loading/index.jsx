import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';

const Loading = ({ isSmall }) => (
  <div className="loader">
    <div className={`loader_element ${isSmall && 'loader_element-small'}`} />
  </div>
);

Loading.propTypes = {
  isSmall: PropTypes.bool
};

Loading.defaultProps = {
  isSmall: false
};

export default Loading;
