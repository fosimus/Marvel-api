import React from 'react';
import PropTypes from 'prop-types';
import LazyLoadImg from '~/components/LazyLoadImg';
import ModalHOC from '~/components/Modal';
import ModalCharacter from './Modal';
import './styles.less';

const Modal = ModalHOC(ModalCharacter);

const Character = ({
  name,
  description,
  urls,
  thumbnail: { path, extension },
  isFakeCharacter
}) => (
  <div className="character">
    <Modal
      bodyProps={{
        name,
        description,
        image: path && `${path}/portrait_incredible.${extension}`,
        urls
      }}
      trigger={
        <div
          className={`character_logo ${
            isFakeCharacter ? 'character_logo-not-active' : ''
          }`}
          // prevent click on fake character
          clickable={isFakeCharacter ? 0 : 1}
        >
          <LazyLoadImg
            srcLoaded={path && `${path}/standard_xlarge.${extension}`}
          />
        </div>
      }
    />
    <div className="character_name">{name}</div>
  </div>
);

Character.propTypes = {
  description: PropTypes.string,
  name: PropTypes.string,
  thumbnail: PropTypes.shape({
    extension: PropTypes.string,
    path: PropTypes.string
  }),
  isFakeCharacter: PropTypes.bool,
  urls: PropTypes.arrayOf(
    PropTypes.shape({
      type: PropTypes.string,
      url: PropTypes.string
    })
  )
};

Character.defaultProps = {
  description: '',
  name: '',
  thumbnail: {
    extension: '',
    path: ''
  },
  isFakeCharacter: false,
  urls: [
    {
      type: '',
      url: ''
    }
  ]
};

export default Character;
