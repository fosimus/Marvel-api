import React from 'react';
import PropTypes from 'prop-types';
import LazyLoadImg from '~/components/LazyLoadImg';
import './ModalStyles.less';

const createLinks = urls =>
  urls.map(el => (
    <a
      key={el.url + el.type}
      className="modal-character_link link"
      href={el.url}
      target="_blank"
    >
      {el.type}
    </a>
  ));

const ModalCharacter = ({
  name,
  image,
  description,
  urls,
  isOpen: isActive
}) => (
  <div className="modal-character">
    <div className="modal-character_name">{name}</div>
    <div className="modal-character_image">
      {isActive && <LazyLoadImg srcLoaded={image} />}
    </div>
    <div className="modal-character_desc">{description}</div>
    <div className="modal-character_links">{createLinks(urls)}</div>
  </div>
);

ModalCharacter.propTypes = {
  name: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  urls: PropTypes.arrayOf(
    PropTypes.shape({
      url: PropTypes.string,
      type: PropTypes.string
    }).isRequired
  ).isRequired,
  isOpen: PropTypes.bool
};

ModalCharacter.defaultProps = {
  isOpen: false
};

export default ModalCharacter;
