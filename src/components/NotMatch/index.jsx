import React from 'react';
import Page from '~/components/ContentPage';

const NotMatch = () => (
  <Page title="Page not found!">
    <div className="no-elements">Try something else...</div>
  </Page>
);

export default NotMatch;
