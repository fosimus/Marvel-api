import React, { Component } from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash.debounce';
import './styles.less';

// we use not stateless component because of debounce
class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
    this.onSearchChange = debounce(this.props.onSearchChange, 500);
  }

  onChacnge = ({ target: { value } }) => {
    this.setState({ value }, () => {
      this.onSearchChange(value);
    });
  };

  render() {
    const { disabled } = this.props;
    return (
      <div className="search">
        <input
          className="search_input input input-search"
          type="text"
          placeholder="Please type something to start searching"
          value={this.state.value}
          disabled={disabled}
          onChange={this.onChacnge}
        />
      </div>
    );
  }
}

Search.propTypes = {
  disabled: PropTypes.bool,
  onSearchChange: PropTypes.func.isRequired
};

Search.defaultProps = {
  disabled: false
};

export default Search;
