import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';

const Content = props => (
  <div className="content">
    <div className="content_wrapper">{props.children}</div>
  </div>
);

Content.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default Content;
