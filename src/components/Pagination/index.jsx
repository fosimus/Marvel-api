import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import './styles.less';

class Pagination extends Component {
  setNextPage = () => {
    // do not worry there is no way to change state if next-button is disabled
    this.props.onPageChange(this.props.options.page + 1);
  };

  setPrevPage = () => {
    // do not worry there is no way to change state if prev-button is disabled
    this.props.onPageChange(this.props.options.page - 1);
  };

  render() {
    const {
      options: { page, totalItems, itemsPerPage },
      disabled,
      show
    } = this.props;
    const pages = Math.ceil(totalItems / itemsPerPage);

    return (
      <div className="pagination">
        {show && (
          <Fragment>
            <input
              type="button"
              className="input input-btn"
              value="PREV"
              onClick={this.setPrevPage}
              disabled={disabled || page === 1}
            />
            &nbsp;
            <input
              type="button"
              className="input input-btn"
              value="NEXT"
              onClick={this.setNextPage}
              disabled={disabled || page === pages}
            />
          </Fragment>
        )}
      </div>
    );
  }
}

Pagination.propTypes = {
  disabled: PropTypes.bool,
  options: PropTypes.shape({
    page: PropTypes.number,
    totalItems: PropTypes.number,
    itemsPerPage: PropTypes.number
  }).isRequired,
  onPageChange: PropTypes.func.isRequired,
  show: PropTypes.bool
};

Pagination.defaultProps = {
  disabled: false,
  show: false
};

export default Pagination;
