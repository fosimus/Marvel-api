import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';

const Page = ({ title, children }) => (
  <div className="page">
    <div className="page_title">
      <h1>{title}</h1>
    </div>
    <div className="page_body">{children}</div>
  </div>
);

Page.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default Page;
