import React from 'react';
import Page from '~/components/ContentPage';

const NotMatch = () => (
  <Page title="Marvel API">
    Technical scope:
    <ul className="ul">
      <li>
        <strong>React</strong> - because it is simple and powerful JavaScript
        lib for building user interfaces
      </li>
      <li>
        <strong>Redux</strong> - because it helps to manage ever-changing state
        easily
      </li>
      <li>
        <strong>React-Router</strong> - because it helps handle Sing Page
        Applications
      </li>
      <li>
        <strong>Webpack</strong> - because it is a perfect bundler for many
        things :)
      </li>
      <li>
        <strong>Flexbox</strong> for views - because it is a good approach for
        creating page layouts
      </li>
      <li>
        <strong>Eslint</strong> with airbnb rules - because all devs should
        follow the same rules
      </li>
    </ul>
    if you want to see my face 😄 👉{' '}
    <a
      className="link"
      href="https://www.instagram.com/fos_andy/"
      target="_blank"
      rel="noopener noreferrer"
    >
      Instagram
    </a>
    <a
      className="link"
      href="https://www.facebook.com/ushakov.andrei"
      target="_blank"
      rel="noopener noreferrer"
    >
      Facebook
    </a>
    <a
      className="link"
      href="https://www.linkedin.com/in/ushakovandrey/"
      target="_blank"
      rel="noopener noreferrer"
    >
      Linkedin
    </a>
  </Page>
);

export default NotMatch;
