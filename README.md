# Marvel API

This is a simple React web application containing five routes:

1. Main page:
    * Simple description about the app.
2. Signin page:
    * Signin form with fake data (reqres.in).
    * Simple validation for email and password.
3. Signup page:
    * Signup form with fake data (reqres.in).
    * Simple validation for email, password and password confirmation.
4. Characters page:
    * Available just for logged users.
    * There is an opportunity to search characters.
    * Also, a user can open a popup with more description about some character by clicking on an image.
5. Not Found page.
    
## Technical scope
* React 16;
* Redux 3;
* React-Router 4;
* Webpack 4;
* Flexbox for views;
* Eslint with airbnb rules;
* etc (check package.json)

## Running (Node v6.11.5 required)

Create `config.js` file in main folder and setup publicKey and privateKey from developer.marvel.com/account

```
export const publicKey = 'Your public key';
export const privateKey = 'Your private key';
```


```
# Install node modules
yarn
```


```
# Start dev server on localhost:8080
yarn start
```


```
# Build dev version
yarn dev
//if you want to test it use simple server such as python:
cd dist && python -m SimpleHTTPServer 8000
```


```
# Build prod version
yarn build
# if you want to test it use some simple server such as python:
cd dist && python -m SimpleHTTPServer 8000
```
